import React, { useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  selectAllItems,
  fetchDiscoverMoviesOrTvShows,
  removeAllItems,
  typeOfSeries,
  updatePage,
  selectPage,
  selectStatusDiscover,
} from "../../redux/Slices/Film";
import NormalCard from "../../component/Card/NormalCard";
import CardSkeleton from "../../component/Card/CardSkeleton";
import Filter from "../../component/Filter/Filter";
import { useNavigate } from "react-router-dom";
import useQueryString from "../../hooks/useQueryString";
import Header from "../../component/Header/Header";
import { CircularProgress } from "@mui/material";

export default function Discover() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const currentParams = useQueryString();
  const { sort, with_genres, start, end } = currentParams;
  const validItem = useSelector(selectAllItems)?.filter(
    (data) => data?.poster_path && data?.backdrop_path
  );
  const type = useSelector(typeOfSeries);
  const page = useSelector(selectPage);
  const status = useSelector(selectStatusDiscover);

  const increasePage = useCallback(() => {
    dispatch(updatePage());
  }, [dispatch]);

  useEffect(() => {
    dispatch(fetchDiscoverMoviesOrTvShows({ sort, with_genres, start, end }));
  }, [dispatch, page, type, sort, with_genres, start, end]);

  useEffect(() => {
    window.scrollTo(0, 0);
    return () => {
      dispatch(removeAllItems());
    };
  }, [dispatch]);
  return (
    <main className="h-full p-1 xl:ml-56">
      <Filter />
      <div className="big-box mt-1 md:mt-0">
        <Header
          setIsLoading={() => {
            dispatch(removeAllItems());
            // setIsLoading(true);
            navigate("/discover");
          }}
        />
        <div className="grid-layout">
          {status === "pending" && page === 1 && validItem?.length === 0 ? (
            <CardSkeleton
              cards={20}
              css="mx-1 h-64 md:h-56 xl:h-60 rounded-lg"
            />
          ) : (
            validItem?.map((r, index) => (
              <NormalCard
                key={index}
                data={r}
                increasePage={increasePage}
                isLastItem={index === validItem.length - 1}
              />
            ))
          )}
        </div>
        {status === "pending" && page > 1 && (
          <CircularProgress className="mx-auto mt-3" />
        )}
        {status === "fullfiled" && validItem?.length === 0 && (
          <div className="flex flex-col items-center justify-center mt-4">
            <h1 class="text-9xl font-bold text-purple-400">404</h1>
            <p className="md:text-lg lg:text-xl text-gray-800 mt-3">
              Sorry, the items you are looking for could not be found.
            </p>
          </div>
        )}
      </div>
    </main>
  );
}
