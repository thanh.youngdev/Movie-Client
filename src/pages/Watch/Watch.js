import React, { memo, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import {
  fetchEmbeddedMovieVideo,
  fetchEmbeddedTVShowsVideo,
} from "../../services/ApiRequests";
import MuiNav from "./MuiNav";
import Recommend from "./Recommend";
import { Skeleton } from "@mui/material";
import useQueryString from "../../hooks/useQueryString";
import {
  fetchDetailOfOneMovieOrTvShows,
  removePrevItem,
  selectDetails,
} from "../../redux/Slices/Detail";

 function Watch() {
  const { season, episode } = useQueryString();
  const { type, id } = useParams();
  const dispatch = useDispatch();
  const detailData = useSelector(selectDetails);
  const [video, setVideo] = useState();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const history = JSON.parse(localStorage.getItem("HISTORY"));
    if (history && history?.length > 0) {
      const isAvailable = history?.find((item) => item.id === Number(id));
      if (!isAvailable?.id) {
        const updateHistory = [...history, detailData];
        localStorage.setItem("HISTORY", JSON.stringify(updateHistory));
      }
    } else {
      if (Object.keys(detailData)?.length > 0) {
        const newArray = [detailData];
        localStorage.setItem("HISTORY", JSON.stringify(newArray));
      }
    }
  }, [detailData, id]);

  useEffect(() => {
    const asyncFetchingEmbeddedVideo = async () => {
      setIsLoading(true);
      window.scrollTo({
        top: 0,
        behavior: "smooth",
      });
      dispatch(fetchDetailOfOneMovieOrTvShows({ type, id }));
      switch (type) {
        case "movie":
          setVideo(fetchEmbeddedMovieVideo(id));
          break;
        case "tv":
          setVideo(fetchEmbeddedTVShowsVideo(id, season, episode));
          break;
        default:
          console.log("some errors in default case");
          return;
      }
      setTimeout(() => {
        setIsLoading(false);
      }, 500);
    };
    asyncFetchingEmbeddedVideo();
  }, [id, episode, season, type, dispatch]);

  useEffect(() => {
    return () => {
      dispatch(removePrevItem());
    };
  }, [dispatch]);

  return (
    <main className="space-y-6 right-component">
      <div className="space-y-4 2xl:basis-1/2">
        {!isLoading && video ? (
          <iframe
            src={video}
            allowFullScreen
            className="aspect-[4/3] h-auto mx-auto mt-3 w-full lg:aspect-video"
            frameBorder="0"
            title="video"
          />
        ) : (
          <Skeleton
            className="aspect-[4/3] h-auto mx-auto mt-3 w-full lg:aspect-video"
            sx={{ bgcolor: "grey.400" }}
            variant="rectangular"
          />
        )}
      </div>
      <div className="shadow shadow-gray-400 bg-[#fbfbfb] h-72 lg:h-80 overflow-y-scroll scroll-smooth">
        <MuiNav setIsLoading={setIsLoading} detailData={detailData} />
      </div>
      <Recommend />
    </main>
  );
}
export default memo(Watch)