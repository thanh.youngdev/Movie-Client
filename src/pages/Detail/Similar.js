import React, { memo, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { selectSimilar, updateSimilarItem } from "../../redux/Slices/Detail";
import { fetchSimilarItems } from "../../services/ApiRequests";
import { useNavigate, useParams } from "react-router-dom";
import { TMBD_IMG_URL } from "../../utils/constants";
import CardSkeleton from "../../component/Card/CardSkeleton";
import {
  LazyLoadImage,
  trackWindowScroll,
} from "react-lazy-load-image-component";
import Star from "../../component/Star/Star";

function Similar({ scrollPosition }) {
  const [isLoading, setIsLoading] = useState(true);
  const navigate = useNavigate();
  const { type, id } = useParams();
  const dispatch = useDispatch();
  const similarItems = useSelector(selectSimilar)?.filter(
    (data) => data?.backdrop_path && data?.poster_path
  );
  useEffect(() => {
    const asyncFetchingSimilarItems = async () => {
      setIsLoading(true);
      try {
        const { results } = await fetchSimilarItems(type, id);
        dispatch(updateSimilarItem(results));
      } catch (error) {
        console.log(error);
      } finally {
        setIsLoading(false);
      }
    };
    asyncFetchingSimilarItems();
  }, [dispatch, type, id]);
  return (
    <div className="mt-3">
      <h1 className="text-xl font-medium py-3">Similar</h1>
      <div className="flex items-center">
        <ul className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-2 lg:gap-x-3">
          {isLoading && <CardSkeleton cards={12} css="h-24 w-44 lg:w-52" />}
          {!isLoading && similarItems && similarItems.length > 0 ? (
            similarItems.map((c) => (
              <li
                onClick={() => navigate(`/${type}/${c.id}`)}
                key={c.id}
                className="flex flex-col space-y-1 cursor-pointer rounded-md"
              >
                <div className="bg-gray-700 relative rounded-md">
                  <LazyLoadImage
                    className={`hover:translate-x-1 hover:-translate-y-1 duration-100 rounded-md brightness-90`}
                    alt=""
                    delayTime={200}
                    src={`${TMBD_IMG_URL("w400")}${c.backdrop_path}`}
                    scrollPosition={scrollPosition}
                  />
                  <span className="text-xs text-slate-100 absolute font-bold top-2 right-2 inline-flex">
                    <Star vote_average={c?.vote_average} />
                  </span>
                </div>
                <p
                  className={`hover:text-[#893218] text-sm font-semibold text-gray-800 pt-0.5 text-clip`}
                >
                  {(c.title || c.name)?.length > 50
                    ? (c.title || c.name)?.substring(0, 50) + `...`
                    : c.title || c.name}
                </p>
                <p className="hover:text-[#893218] text-sm text-gray-800 pt-0.5">
                  {c.release_date || c.first_air_date}
                </p>
              </li>
            ))
          ) : (
            <p className="text-left">No data Found</p>
          )}
        </ul>
      </div>
    </div>
  );
}
export default memo(trackWindowScroll(Similar));
