import React from "react";
import CompactBar from "../CompactBar/CompactBar";
import Footer from "../Footer/Footer";
import LeftItem from "../LeftItem/LeftItem";

export default function Layout({ children }) {
  return (
    <div className="overflow-hidden">
      <div className="bg-[#EDF1F5] w-screen flex">
        <CompactBar>
          <LeftItem />
        </CompactBar>
        <section className="w-full h-full">
          {children}
          <Footer />
        </section>
      </div>
    </div>
  );
}
