import React, { memo } from "react";
import { Link } from "react-router-dom";
import { TMBD_IMG_URL } from "../../utils/constants";
import Star from "../Star/Star";

function BarCard({ type, results }) {
  return (
    <Link to={`/${type}/${results.id}`}>
      <div className="duration-400 transition h-28 flex cursor-pointer rounded-lg space-x-2 flex-row items-center shadow-gray-400 bg-gradient-to-r from-gray-700 to-gray-900 text-white shadow-lg hover:-translate-y-1 hover:shadow-gray-900">
        <img
          className="h-full w-[35%] shrink-0 rounded-l-lg object-fill"
          src={`${TMBD_IMG_URL("w200")}${results.poster_path}`}
          alt=""
        />
        <div className="flex flex-col justify-around grow h-full">
          {(results.title || results.name).length > 40 ? (
            <p className={`text-sm font-mediunm text-clip`}>
              {(results.title || results.name).slice(0, 40)}...
            </p>
          ) : (
            <p className={`text-sm font-mediunm text-clip`}>
              {results.title || results.name}
            </p>
          )}
          <Star vote_average={results?.vote_average} />
          <p className="text-xs">
            {results.release_date || results.first_air_date}
          </p>
        </div>
      </div>
    </Link>
  );
}

export default memo(BarCard);
