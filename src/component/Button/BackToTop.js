import React, { useCallback, useEffect, useState } from "react";

export default function BackToTop() {
  const [backToTop, setBackToTop] = useState(false);

  const scrollUp = useCallback(() => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  }, []);

  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY > 150) {
        setBackToTop(true);
      } else {
        setBackToTop(false);
      }
    };
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  return (
    <>
      {backToTop && (
        <div
          onClick={scrollUp}
          className="fixed bottom-16 md:bottom-7 md:right-5 mb-6 md:mb-0 right-3 p-2 bg-[#ffffff] z-50 hover:brightness-75 cursor-pointer"
        >
          <img
            className="w-6 h-6 object-contain"
            src="https://cdn-icons-png.flaticon.com/512/307/307481.png"
            alt=""
          ></img>
        </div>
      )}
    </>
  );
}
