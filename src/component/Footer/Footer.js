import React from "react";
import { useMatch } from "react-router-dom";

export default function Footer() {
  const isMatchHome = Boolean(useMatch("/"));
  return (
    <footer
      className={`footer text-center mt-5 px-3 pt-1 bg-gray-300 text-gray-800 ${
        !isMatchHome && "xl:ml-56"
      }`}
    >
      <div className="text-center">
        <p className="text-base self-center p-3.5">Copyright © T-Movie © 2023</p>
      </div>
    </footer>
  );
}
