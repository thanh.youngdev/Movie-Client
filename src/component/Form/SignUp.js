import { CircularProgress } from "@mui/material";
import React, { useEffect, useState } from "react";
import { BsEyeSlash } from "react-icons/bs";
import { FiEye } from "react-icons/fi";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import userService from "../../services/User";
import { toastOptions } from "../../utils/options";
import { validateInput } from "../../utils/ValidateInput";

export default function SignUp() {
  const initialValues = {
    userName: "",
    email: "",
    password: "",
  };
  const navigate = useNavigate();
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmit, setIsSubmit] = useState(false);
  const [passwordShown, setPasswordShown] = useState(false);
  const overLay = isSubmit && Object.keys(formErrors).length === 0;

  const handleChangeInput = (e) => {
    setFormValues({ ...formValues, [e.target.name]: e.target.value });
  };

  const togglePasswordVisiblity = () => {
    setPasswordShown(passwordShown ? false : true);
  };

  const handleSubmitForm = (e) => {
    e.preventDefault();
    setFormErrors(validateInput(formValues));
    setIsSubmit(true);
  };

  useEffect(() => {
    if (Object.keys(formErrors).length === 0 && isSubmit) {
      userService
        .registerUser(formValues)
        .then((res) => {
          toast.success("Register successfully!", toastOptions(1000));
          setTimeout(() => {
            setIsSubmit(false);
            navigate("/auth/login");
          }, 1500);
        })
        .catch((err) => {
          setIsSubmit(false);
          toast.error(err?.response?.data || err?.message, toastOptions(1500));
        })
    }
  }, [formErrors,isSubmit,navigate,formValues]);

  return (
    <div className="min-h-screen flex items-center justify-center">
      <form onSubmit={handleSubmitForm} className="form">
        <div className="py-10 px-6 rounded-2xl">
          <h3 className="font-semibold mb-4 text-2xl text-center text-black">
            Sign Up
          </h3>
          <div className="space-y-2">
            <div className="space-y-2">
              <label className="text-sm font-medium text-black tracking-wide">
                Username
              </label>
              <input
                name={`userName`}
                onChange={(e) => handleChangeInput(e)}
                className=" w-full text-base px-4 py-2 border border-gray-300 h-[45px] rounded focus:outline-none focus:border focus:border-blue-500"
                type={`text`}
                placeholder="Enter your User Name"
              />
              <p className="text-red-600 text-xs">{formErrors?.userName}</p>
            </div>
            <div className="space-y-2">
              <label className="text-sm font-medium text-black tracking-wide">
                Email
              </label>
              <input
                name={`email`}
                onChange={(e) => handleChangeInput(e)}
                className=" w-full text-base px-4 py-2 border border-gray-300 h-[45px] rounded focus:outline-none focus:border focus:border-blue-500"
                type={`text`}
                placeholder="Enter your email"
              />
              <p className="text-red-600 text-xs">{formErrors?.email}</p>
            </div>
            <div className="space-y-2 relative">
              <label className="mb-5 text-sm font-medium text-black ">
                Password
              </label>
              <input
                name={`password`}
                onChange={(e) => handleChangeInput(e)}
                className="w-full content-center text-base px-4 py-2 border border-gray-300 h-[45px] rounded focus:outline-none focus:border focus:border-blue-500"
                type={passwordShown ? "text" : "password"}
                placeholder="Enter your password"
              />
              <i
                className="absolute cursor-pointer top-0 right-0"
                onClick={togglePasswordVisiblity}
              >
                {passwordShown ? <BsEyeSlash /> : <FiEye />}
              </i>
              <p className="text-red-600 text-xs">{formErrors?.password}</p>
            </div>
            <div className="flex items-center justify-between"></div>
            <div className="relative w-full">
              <button
                disabled={overLay}
                type="submit"
                className={`w-full cursor-pointer flex justify-center ${
                  !overLay && "bg-green-400 hover:bg-green-500"
                } text-gray-100 p-3 rounded-full`}
              >
                Submit
              </button>
              {overLay && (
                <div className="absolute flex justify-center items-center w-full h-full cursor-not-allowed z-50 bg-white bg-opacity-40 backdrop-blur-md drop-shadow-lg top-0 left-0 rounded-full">
                  <CircularProgress size={20} />
                </div>
              )}
            </div>
            <div className="text-sm flex justify-center mt-3 space-x-2 ">
              <span>Or:</span>
              <button
                onClick={(e) => {
                  e.preventDefault();
                  navigate("/auth/login");
                }}
              >
                Sign In
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
}
