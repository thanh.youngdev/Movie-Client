import React, { useCallback, useState } from "react";
import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import { toast } from "react-toastify";
import { MdHistory } from "react-icons/md";
import PopUp from "./PopUp";
import { useNavigate, useParams } from "react-router-dom";
import { toastOptions } from "../../utils/options";
import ModalCard from "../Card/ModalCard";
import { CircularProgress } from "@mui/material";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  overflow: "scroll",
  width: 400,
  height: 350,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
};

export default function History({ css }) {
  const [open, setOpen] = useState(false);
  const [isLoading, setIsloading] = useState(true);
  const navigate = useNavigate();
  const { id } = useParams();

  const handleClose = useCallback(() => {
    setIsloading(true);
    setOpen(false);
  }, []);

  const [historyItems, setHistoryItems] = useState([]);
  const handleClickItem = () => {
    setOpen(true);
    const data = JSON.parse(localStorage.getItem("HISTORY")) ?? [];
    setHistoryItems(data);
    setTimeout(() => {
      setIsloading(false);
    }, 500);
  };
  const handleNavigate = (data) => {
    setOpen(false);
    if (id === data.id) {
      return;
    }

    if ("seasons" in data) {
      navigate(`/tv/${data.id}`);
    } else {
      navigate(`/movie/${data.id}`);
    }
  };
  const handleRemoveAll = () => {
    const getHistory = JSON.parse(localStorage.getItem("HISTORY")) || [];
    if (getHistory?.length === 0) {
      toast.warning("You have not watched any films yet", toastOptions(1500));
      return;
    }
    const emptyArr = [];
    setHistoryItems(emptyArr);
    localStorage.setItem("HISTORY", JSON.stringify(emptyArr));
    toast.success("Remove successfully", toastOptions(1500));
  };

  const handleDeleteItem = (id) => {
    const History = JSON.parse(localStorage.getItem("HISTORY"));
    if (History) {
      const newHistory = History.filter((h) => h.id !== id);
      setHistoryItems(newHistory);
      localStorage.setItem("HISTORY", JSON.stringify(newHistory));
      toast.success("Remove successfully", toastOptions(1500));
    }
  };
  return (
    <>
      <button
        className={`${css ? css : "item-left-bar"}`}
        onClick={handleClickItem}
      >
        <MdHistory />
        <span className={`${css ? "block text-xs pb-2" : "ml-2"}`}>
          History
        </span>
      </button>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box className="flex flex-col space-y-3 rounded" sx={style}>
            <div className="flex justify-between items-center">
              <h2 className="text-lg font-semibold">History</h2>
              <button
                onClick={handleRemoveAll}
                className="px-3 py-1.5 text-slate-400 hover:text-white bg-slate-800 text-sm font-semibold rounded-lg transition-all duration-500 ease-in-out"
              >
                <i className="uil uil-users-alt mr-1" /> Remove All
              </button>
            </div>
            {isLoading && (
              <div className="flex justify-center">
                <CircularProgress />
              </div>
            )}
            {!isLoading && historyItems?.length > 0 ? (
              historyItems?.map((h) => (
                <ModalCard
                  key={h.id}
                  data={h}
                  handleDeleteItem={handleDeleteItem}
                  handleNavigate={handleNavigate}
                />
              ))
            ) : !isLoading ? (
              <PopUp handleClose={handleClose} />
            ) : undefined}
          </Box>
        </Fade>
      </Modal>
    </>
  );
}
