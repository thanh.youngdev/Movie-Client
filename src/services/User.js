import axios from "axios";
import { toast } from "react-toastify";
import { toastOptions } from "../utils/options";
import { localStorageService } from "./LocalStorage";
import { axiosClient } from "./_configURL";

const userService = {
  getProfile: async function () {
    try {
      const { data } = await axiosClient.get("/user/profile");
      return data;
    } catch (error) {
      toast.error(error?.response?.data || error?.message, toastOptions(1500));
    }
  },
  deleteAccount: async function (currentPass) {
    try {
      const { data } = await axiosClient.post(
        "/user/deleteAccount",
        {
          confirmPass: currentPass,
        },
        {
          withCredentials: true,
        }
      );
      return data;
    } catch (error) {
      toast.error(error?.response?.data || error?.message, toastOptions(1500));
    }
  },
  logIn: async function (formValues) {
    const userCredentials = {
      email: formValues.email.toLowerCase(),
      password: formValues.password.toLowerCase(),
    };
    const { data } = await axios.post(
      `${process.env.REACT_APP_SEVER}/auth/login`,
      userCredentials,
      {
        withCredentials: true,
      }
    );
    return data;
  },
  registerUser: async function (credentials) {
    const { userName, email, password } = credentials;
    const { data } = await axios.post(
      `${process.env.REACT_APP_SEVER}/auth/register`,
      {
        username: userName.toLowerCase().trim(),
        email: email.toLowerCase(),
        password: password.toLowerCase(),
      }
    );
    return data;
  },
  logOut: async function () {
    await axiosClient.post("/auth/logout", undefined, {
      withCredentials: true,
    });
    toast.success("Logout Successfully", toastOptions(1500));
    localStorageService.user.remove();
  },
};

export default userService;
