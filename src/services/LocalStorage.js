const USER_DATA = "USER_DATA";
// const HISTORY = "HISTORY";

export const localStorageService = {
  user: {
    set: function (user) {
      const dataJSON = JSON.stringify(user);
      localStorage.setItem(USER_DATA, dataJSON);
    },
    get: function () {
      const dataJSON = localStorage.getItem(USER_DATA)
        ? JSON.parse(localStorage.getItem(USER_DATA))
        : {};
      return dataJSON;
    },
    remove: function () {
      localStorage.removeItem(USER_DATA);
      // localStorage.removeItem(HISTORY);
    },
  },
};
