export const API_KEY = process.env.REACT_APP_API_KEY;

export const BASE_URL = `https://api.themoviedb.org/3`;

export const TMBD_IMG_URL = (size) => `https://image.tmdb.org/t/p/${size}`;
